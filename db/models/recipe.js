const { Model } = require('objection');

class Recipe extends Model {
  static get tableName() {
    return 'recipe';
  }
}

module.exports = Recipe;