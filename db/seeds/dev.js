exports.seed = async function (knex) {
    // truncate all existing tables
    await knex.raw('TRUNCATE TABLE "recipe" CASCADE');
  
    // insert seed data
    await knex('recipe').insert([
      {
        id: 1,
        name: 'maggie',
        'ing':'1 pck maggie & water',
        'directions':'add water boit it and then add maggie'
      },
      {
        id: 2,
        name: 'oats',
        'ing':'1 pck oats & water',
        'directions':'add water boit it and then add oats'
      },
    ]);
  
    
  };