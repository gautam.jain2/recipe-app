
exports.up = async function(knex) {
    return knex.schema.createTable('recipe',(table)=>{
        table.increments();
        table.string('name').notNullable;
        table.string('ing').notNullable;
        table.string('directions').notNullable;
        table.timestamps(true,true);
    })
  
};

exports.down = function(knex) {
    return knex.schema
    .dropTableIfExists('recipe');
  
};
