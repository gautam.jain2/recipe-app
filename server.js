const express=require('express')
const path=require('path')
const bodyParser=require('body-parser')
const cons=require('consolidate')
const dust=require('dustjs-helpers')
const pg=require('pg')
const dbSetup=require('./db/db-setup')
const Recipe=require('./db/models/recipe.js')
dbSetup();

const app=express()
app.engine('dust',cons.dust);
app.set('view engine','dust');
app.set('views',__dirname+'/views')
app.use(express.static(path.join(__dirname,'public')));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}));
app.get('/',async (req,res)=>{
    try{
        const { id }=req.params;
        const recipe=await Recipe.query();
        res.render('index',{recipes:recipe});

    }
    catch(err){
        console.error(err);
        res.status(500).json(err);
    }
   
})
app.post('/add',async function(req,res){  
    try{
        const recip=await Recipe.query();
        const recp = await Recipe.query().insert({
            id:recip.length+1,
            name: req.body.name,
            ing: req.body.ing,
            directions:req.body.directions
          });
          console.log(recp instanceof Recipe);
          res.redirect('/')

    }
    catch(err){
        console.error(err);
        res.status(500).json(err);
    }
   
      

});
app.post('/edit',async function(req,res){
    try{
        const recp = await Recipe.query()
  .findById(req.body.id)
  .patch({
    name: req.body.name,
            ing: req.body.ing,
            directions:req.body.directions
  });
          console.log(recp instanceof Recipe);
          res.redirect('/')

    }
    catch(err){
        console.error(err);
        res.status(500).json(err);
    }

})
app.post('/delete',async function(req,res){
    try{
        const recp = await Recipe.query().deleteById(req.body.id);
          console.log(recp instanceof Recipe);
          res.redirect('/')

    }
    catch(err){
        console.error(err);
        res.status(500).json(err);
    }

})
app.listen(3000)